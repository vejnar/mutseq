#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Copyright (C) 2015-2017 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import argparse
import csv
import json
import logging
import os
import subprocess
import sys

import mutseq.align
import mutseq.genetic
import mutseq.report

def main(argv=None):
    if not argv:
        argv = sys.argv
    parser = argparse.ArgumentParser(description='Prediction of sgRNA targets.')
    parser.add_argument('-l', '--path_loci', dest='path_loci', action='store', default='loci.csv', help='Path to loci CSV file.')
    parser.add_argument('-i', '--path_index', dest='path_index', action='store', default='index', help='Path to mapping index.')
    parser.add_argument('-s', '--path_aligning', dest='path_aligning', action='store', default='aligning', help='Path to read alignments.')
    parser.add_argument('-d', '--path_data', dest='path_data', action='store', default='.', help='Path to sequencing data.')
    parser.add_argument('-m', '--map_soft', dest='map_soft', action='store', default='star', help='Mapping software (gmap or star).')
    parser.add_argument('-w', '--two_pass', dest='two_pass', action='store_true', default=False, help='Two-pass alignment (with SSW).')
    parser.add_argument('-c', '--cat_cmd', dest='cat_cmd', action='store', default='zcat', help='Program to stream data to mapped (cat, zcat or bzcat etc).')
    parser.add_argument('-t', '--trim_max', dest='trim_max', action='store', type=int, default=3, help='Maximum length of PCR oligo trimming.')
    parser.add_argument('-a', '--min_padding', dest='min_padding', action='store', type=int, default=6, help='Minimum length of alignment in addition to PCR oligo.')
    parser.add_argument('-n', '--min_allele_percent', dest='min_allele_percent', action='store', type=float, default=1., help='Minimum percentage of reads to report an allele.')
    parser.add_argument('-r', '--min_allele_reads', dest='min_allele_reads', action='store', type=int, default=1000, help='Minimum number of reads to report an allele.')
    parser.add_argument('-y', '--target', dest='target', action='store_true', default=False, help='Consider only allele with target site (Use the "target_seq" column in loci CSV file).')
    parser.add_argument('-o', '--report', dest='report', action='store', default='-', help='Path to report file or "-" for stdout.')
    parser.add_argument('-j', '--report_json', dest='report_json', action='store', default='', help='Path to JSON report file or "-" for stdout.')
    parser.add_argument('-g', '--log', dest='log', action='store', default='INFO', help='Log level: INFO, DEBUG etc.')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='Verbose.')
    parser.add_argument('-p', '--processor', dest='num_processor', action='store', type=int, default=1, help='Number of processor.')
    args = parser.parse_args(argv[1:])

    # Logging
    logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%H:%M', level=args.log)

    # Parse loci CSV file
    loci = []
    with open(args.path_loci, newline='') as csvfile:
        cr = csv.reader(csvfile)
        # Header
        header = next(cr)
        missing_column = set(mutseq.genetic.get_locus_required_columns(args.target)).difference(set(header))
        if len(missing_column) > 0:
            logging.error('Missing column(s) in %s: %s'%(args.path_loci, ','.join(missing_column)))
            return 1
        # Read loci
        for row in cr:
            loci.append(mutseq.genetic.new_locus(dict(zip(header, row))))
    # Samples
    samples = []
    for sample in [sample for locus in loci for sample in locus.samples]:
        if sample not in samples:
            samples.append(sample)
    logging.info('Parsing %s: %i loci and %i sample(s) found.'%(args.path_loci, len(loci), len(samples)))

    # Create index
    if os.path.exists(os.path.join(args.path_index)):
        logging.info('Index already exists in %s'%(args.path_index))
    else:
        logging.info('Creating index in %s'%(args.path_index))
        # Create index folder
        os.mkdir(args.path_index)
        # Create FASTA
        with open(os.path.join(args.path_index, 'seq.fa'), 'wt') as f:
            for locus in loci:
                f.write('>%s\n%s\n'%(locus.name, locus.seq))
        if args.two_pass:
            for locus in loci:
                with open(os.path.join(args.path_index, 'seq_%s.fa'%locus.name), 'wt') as f:
                    f.write('>%s\n%s\n'%(locus.name, locus.seq))
        # Create index
        if args.map_soft == 'gmap':
            mutseq.align.index_gmap('seq.fa', args.path_index)
        elif args.map_soft == 'star':
            mutseq.align.index_star('seq.fa', args.path_index, len(loci), sum([len(s) for s in locus.seq]))

    # Map & Allele detection
    data_report = {}
    for sample in samples:
        # Map and align reads
        path_sample = os.path.join(args.path_data, sample)
        if os.path.exists(sample + '_R1.sam') or os.path.exists(os.path.join(args.path_aligning, sample)):
            logging.info('Sample %s already aligned'%(sample))
        else:
            logging.info('Aligning %s'%(sample))
            if args.map_soft == 'gmap':
                mutseq.align.align_gmap(sample,
                                        path_in = args.path_data,
                                        path_index = args.path_index,
                                        path_aligning = args.path_aligning,
                                        cat_cmd = args.cat_cmd,
                                        num_processor = args.num_processor)
            elif args.map_soft == 'star':
                mutseq.align.align_star(sample,
                                        path_in = args.path_data,
                                        path_index = args.path_index,
                                        path_aligning = args.path_aligning,
                                        cat_cmd = args.cat_cmd,
                                        num_processor = args.num_processor)

        # Allele detection
        logging.info('Detecting allele in %s'%(sample))
        loci_alleles, loci_npair, loci_npair_mutant, loci_npair_aligned, loci_npair_wt, npair_unaligned = mutseq.genetic.get_alleles_per_loci(sample, args.path_aligning, loci, args.trim_max, args.min_padding, args.map_soft, int(args.two_pass), '2', args.target, args.verbose)
        if args.two_pass:
            for locus in loci:
                mutseq.align.align_ssw(sample,
                                       locus.name,
                                       path_index = args.path_index,
                                       path_aligning = args.path_aligning + '2',
                                       num_processor = args.num_processor)
            loci_alleles, loci_npair, loci_npair_mutant, loci_npair_aligned, loci_npair_wt, npair_unaligned = mutseq.genetic.get_alleles_per_loci(sample, args.path_aligning, loci, args.trim_max, args.min_padding, args.map_soft, 2, '2', args.target, args.verbose)

        # Report
        sample_stats = mutseq.report.get_sample_stats(sample, loci, loci_alleles, loci_npair, loci_npair_mutant, loci_npair_aligned, loci_npair_wt, npair_unaligned, args.min_allele_percent, args.min_allele_reads)
        data_report.update(sample_stats)
        logging.info('Writing report to %s'%(args.report))
        if args.report == '-':
            fout = sys.stdout
        else:
            fout = open(args.report, 'at')
        mutseq.report.write_report(sample_stats, fout)
        # Close
        if args.report != '-':
            fout.close()

    # JSON report
    if args.report_json != '':
        logging.info('Writing JSON report to %s'%(args.report_json))
        if args.report_json == '-':
            fout = sys.stdout
        else:
            fout = open(args.report_json, 'wt')
        json.dump(data_report, fout)
        # Close
        if args.report_json != '-':
            fout.close()

if __name__ == '__main__':
    sys.exit(main())
