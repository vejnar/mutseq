# -*- coding: utf-8 -*-

#
# Copyright (C) 2015-2017 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import glob
import logging
import math
import os
import subprocess

from . import aln_sam

def index_gmap(path_seq, path_index):
    with open(os.path.join(path_index, 'index.log'), 'wt') as f:
        subprocess.run(['gmap_build', '--kmer', '10', '--dir', '.', '--db', 'index', path_seq], stderr=f, cwd=path_index, check=True)

def align_gmap(sample, path_in, path_index, path_aligning='.', cat_cmd=None, logger=None, num_processor=1):
    # Parameters
    if logger is None:
        logger = logging
    else:
        logger = logger.getChild(logger.name+'.align_gmap')

    for read in ['R1', 'R2']:
        cmd = ['gmap',
               '--dir', path_index,
               '--db', 'index',
               '--min-intronlength', '1600',
               '--allow-close-indels', '2',
               '--npaths', '1',
               '--ordered',
               '--input-buffer-size', '12000',
               '--batch', '4',
               '--format', 'samse',
               '--nthreads', str(num_processor)]
        # Create folder
        if not os.path.exists(path_aligning):
            os.mkdir(path_aligning)
        # Input
        if cat_cmd is not None:
            path_fins = sorted(glob.glob(os.path.join(path_in, sample, '*_%s*.fastq*'%read)))
            if len(path_fins) == 0:
                raise Exception('No input file found')
            logger.info('Loading file(s): %s'%(','.join(path_fins)))
            catp = subprocess.Popen([cat_cmd] + path_fins, stdout=subprocess.PIPE)
            gmap_in = catp.stdout
        else:
            catp = None
            gmap_in = open(os.path.join(path_in, sample+'.fastq'), 'rt')
        # Output
        with open(os.path.join(path_aligning, '%s_%s.sam'%(sample, read)), 'wt') as fout, open(os.path.join(path_aligning, '%s_%s.log'%(sample, read)), 'wt') as ferr:
            logger.info('Starting GMAP (%s,%s) with %s'%(sample, read, cmd))
            p = subprocess.Popen(cmd, stdin=gmap_in, stdout=fout, stderr=ferr)
            if catp is not None:
                catp.wait()
            p.wait()
            if p.returncode != 0:
                raise subprocess.SubprocessError('GMAP failed')

def get_aln_gmap(path_aligning, sample):
    with open(os.path.join(path_aligning, sample + '_R1.sam'), 'rt') as fr1, open(os.path.join(path_aligning, sample + '_R2.sam'), 'rt') as fr2:
        for l1, l2 in zip(fr1, fr2):
            if l1[0].startswith('@'):
                continue
            # Parse SAM
            r1 = aln_sam.AlignedRead(l1)
            r2 = aln_sam.AlignedRead(l2)
            yield r1, r2

def star_string_number(genome_nseq, genome_length):
    return int(math.floor(min(18, math.log2(genome_length) / genome_nseq)))

def star_string_length(genome_length):
    return int(math.floor(min(14, math.log2(genome_length) / 2 - 1)))

def index_star(path_seq, path_index, genome_nseq=None, genome_length=None):
    cmd = ['STAR', '--runMode', 'genomeGenerate', '--genomeDir', '.', '--genomeFastaFiles', path_seq]
    # Genome: Number of sequences
    if genome_nseq is not None:
        cmd.append('--genomeChrBinNbits')
        cmd.append(str(star_string_number(genome_nseq, genome_length)))
    # Genome: Length
    if genome_length is not None:
        cmd.append('--genomeSAindexNbases')
        cmd.append(str(star_string_length(genome_length)))
    # Create STAR index
    subprocess.run(cmd, cwd=path_index, check=True)

def align_star(sample, path_in, path_index, path_aligning='.', cat_cmd=None, logger=None, num_processor=1):
    # Parameters
    if logger is None:
        logger = logging
    else:
        logger = logger.getChild(logger.name+'.align_star')
    # Prepare command
    cmd = ['STAR',
           '--genomeDir', path_index,
           '--outFileNamePrefix', os.path.join(path_aligning, sample) + '/',
           '--outSAMmultNmax', '1',
           '--outSAMattributes', 'All',
           '--outFilterMatchNminOverLread', '0.3',
           '--outFilterScoreMinOverLread', '0.3',
           '--seedSearchStartLmax', '20',
           '--alignEndsType', 'Local',
           '--alignIntronMin', '2000',
           '--scoreDelOpen', '-4',
           '--scoreDelBase', '0',
           '--scoreInsOpen', '-4',
           '--scoreInsBase', '0',
           '--outSAMunmapped', 'Within', 'KeepPairs',
           '--runThreadN', str(num_processor)]
    cmd.append('--readFilesIn')
    cmd.append(','.join(sorted(glob.glob(os.path.join(path_in, sample, '*_R1*.fastq*')))))
    cmd.append(','.join(sorted(glob.glob(os.path.join(path_in, sample, '*_R2*.fastq*')))))
    if cat_cmd is not None:
        cmd.append('--readFilesCommand')
        cmd.append(cat_cmd)
    # Create folder
    if not os.path.exists(path_aligning):
        os.mkdir(path_aligning)
    os.mkdir(os.path.join(path_aligning, sample))
    # Run
    logger.info('Starting STAR (%s) with %s'%(sample, cmd))
    subprocess.run(cmd, check=True)

def get_aln_star(path_aligning, sample):
    with open(os.path.join(path_aligning, sample, 'Aligned.out.sam'), 'rt') as fr:
        for l in fr:
            if l[0].startswith('@'):
                continue
            # Parse SAM
            r1 = aln_sam.AlignedRead(l)
            r2 = aln_sam.AlignedRead(next(fr))
            yield r1, r2

def align_ssw(sample, locus_name, path_index, path_aligning='.', logger=None, num_processor=1):
    # Parameters
    if logger is None:
        logger = logging
    else:
        logger = logger.getChild(logger.name+'.align_ssw')
    # Prepare command
    cmd = ['ssw',
           '-s',
           '-r',
           '-c',
           '-m', '5',
           '-x', '4',
           '-o', '20',
           '-e', '0',
           os.path.join(path_index, 'seq_%s.fa'%locus_name),
           os.path.join(path_aligning, sample, locus_name+'.fa')]
    # Create folder
    if not os.path.exists(path_aligning):
        os.mkdir(path_aligning)
    if not os.path.exists(os.path.join(path_aligning, sample)):
        os.mkdir(os.path.join(path_aligning, sample))
    # Output
    with open(os.path.join(path_aligning, sample, '%s.sam'%locus_name), 'wt') as fout:
        logger.info('Starting SSW (%s,%s) with %s'%(sample, locus_name, cmd))
        p = subprocess.Popen(cmd, stdout=fout)
        p.wait()
        if p.returncode != 0:
            raise subprocess.SubprocessError('SSW failed')

def get_aln_ssw(path_aligning, sample, locus_name):
    with open(os.path.join(path_aligning, sample, locus_name + '.sam'), 'rt') as fr:
        for l in fr:
            if l[0].startswith('@'):
                continue
            # Parse SAM
            r1 = aln_sam.AlignedRead(l)
            r2 = aln_sam.AlignedRead(next(fr))
            yield r1, r2
