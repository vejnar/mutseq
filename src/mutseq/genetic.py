# -*- coding: utf-8 -*-

#
# Copyright (C) 2015-2017 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import logging
import math
import os

from . import align

_locus_required_columns = ['name', 'samples', 'oligo_fw', 'oligo_rv', 'seq']

def get_locus_required_columns(target=False):
    opts = []
    if target:
        opts.append('target_seq')
    return _locus_required_columns + opts

class Locus(object):
    def __init__(self, name, samples, oligo_fw, oligo_rv, seq, target_seq=None):
        self.name = name
        if isinstance(samples, str):
            self.samples = samples.split(',')
        else:
            self.samples = samples
        self.oligo_fw = oligo_fw.strip()
        self.oligo_rv = oligo_rv.strip()
        self.seq = seq.strip()
        if target_seq is not None:
            self.target_seq = target_seq.strip()
            self.target_start = seq.find(self.target_seq)
            if self.target_start == -1:
                self.target_end = -1
            else:
                self.target_end = self.target_start + len(self.target_seq)
        self.oligo_fw_len = len(self.oligo_fw)
        self.oligo_rv_len = len(self.oligo_rv)
        self.seq_len = len(self.seq)

def new_locus(d):
    return Locus(name = d.get('name'),
                 samples = d.get('samples'),
                 oligo_fw = d.get('oligo_fw'),
                 oligo_rv = d.get('oligo_rv'),
                 seq = d.get('seq'),
                 target_seq = d.get('target_seq'))

def is_wt(read, trim_max=3):
    if len(read.cigar) == 1 and (read.cigar[0][0] == 'M' or read.cigar[0][0] == '='):
        return True
    if len(read.cigar) == 2:
        if (read.cigar[0][0] == 'S' and read.cigar[0][1] <= trim_max and (read.cigar[1][0] == 'M' or read.cigar[1][0] == '=')) \
           or ((read.cigar[0][0] == 'M' or read.cigar[0][0] == '=') and read.cigar[1][0] == 'S' and read.cigar[1][1] <= trim_max):
           return True
    return False

def get_indels(read):
    indels = []
    current_rpos = read.pos
    for iop in range(len(read.cigar) - 1):
        if read.cigar[iop][0] == 'D' or read.cigar[iop][0] == 'I':
            indels.append((read.cigar[iop][0], current_rpos, current_rpos+read.cigar[iop][1]))
        # Soft (S) and hard (H) clipping are not included in pos
        if read.cigar[iop][0] == 'M' or read.cigar[iop][0] == '=' or read.cigar[iop][0] == 'D':
            current_rpos += read.cigar[iop][1]
    return indels

def get_alleles_per_loci(sample, path_aligning, loci, trim_max, min_padding, align_software='star', two_pass=0, two_pass_suffix='', within_target=False, verbose=False):
    # Init. loci dict
    locid = {locus.name: locus for locus in loci}
    # Init. stats per locus
    loci_in_samples = [locus for locus in loci if sample in locus.samples]
    loci_alleles = {locus.name: {} for locus in loci_in_samples}
    loci_npair = {locus.name: 0 for locus in loci_in_samples}
    loci_npair_mutant = {locus.name: 0 for locus in loci_in_samples}
    loci_npair_aligned = {locus.name: 0 for locus in loci_in_samples}
    loci_npair_wt = {locus.name: 0 for locus in loci_in_samples}
    npair_unaligned = 0

    # Looping over alignments
    if align_software == 'gmap':
        get_aln = align.get_aln_gmap(path_aligning, sample)
    elif align_software == 'star':
        get_aln = align.get_aln_star(path_aligning, sample)

    # Two-pass: First pass re-alignment output
    if two_pass == 1:
        if not os.path.exists(path_aligning + two_pass_suffix):
            os.mkdir(path_aligning + two_pass_suffix)
        if not os.path.exists(os.path.join(path_aligning + two_pass_suffix, sample)):
            os.mkdir(os.path.join(path_aligning + two_pass_suffix, sample))
        first_pass_out = {locus.name: open(os.path.join(path_aligning + two_pass_suffix, sample, locus.name + '.fa'), 'wt') for locus in loci}
    # Two-pass: Second pass re-alignment input
    if two_pass == 2:
        get_aln_second_pass = {locus.name: align.get_aln_ssw(path_aligning + two_pass_suffix, sample, locus.name) for locus in loci}
        r1b = {locus.name: None for locus in loci}
        r2b = {locus.name: None for locus in loci}

    for r1, r2 in get_aln:
        # Locus
        if r1.rname != '*':
            locus_name = r1.rname
        elif r2.rname != '*':
            locus_name = r2.rname
        else:
            npair_unaligned += 1
            continue

        # Error in input
        if r1.qname != r2.qname:
            logging.error('Read1 and read2 name are different: %s != %s', r1.qname, r2.qname)
            continue
        # Read1 and read2 align on two different loci
        if r1.is_mapped() and r2.is_mapped() and r1.rname != r2.rname:
            loci_npair_aligned[locus_name] += 1
            continue
        if verbose:
            print()

        if two_pass == 2:
            if r1b[locus_name] is None:
                r1b[locus_name], r2b[locus_name] = next(get_aln_second_pass[locus_name])
                # Error in input
                if r1b[locus_name].qname != r2b[locus_name].qname:
                    logging.error('Read1 and read2 name are different: %s != %s', r1b[locus_name].qname, r2b[locus_name].qname)
            if r1.qname == r1b[locus_name].qname:
                r1 = r1b[locus_name]
                r2 = r2b[locus_name]
                try:
                    r1b[locus_name], r2b[locus_name] = next(get_aln_second_pass[locus_name])
                except StopIteration:
                    pass

        pair_status = []
        pair_allele = []
        iread = 1
        for read in [r1, r2]:
            # Unaligned
            if not read.is_mapped():
                pair_status.append('unaligned')
            elif is_wt(read, trim_max=trim_max):
                pair_status.append('wt')
            else:
                mutant = False
                within_target_wt = True
                indels = get_indels(read)
                if len(indels) > 0:
                    mutant = True
                    for etype, start, end in [indels[0], indels[-1]]:
                        if start <= locid[locus_name].oligo_fw_len + min_padding or locid[locus_name].seq_len - locid[locus_name].oligo_rv_len - min_padding <= end:
                            mutant = False
                    if mutant and within_target:
                        for etype, start, end in indels:
                            if not (locid[locus_name].target_start <= start and end <= locid[locus_name].target_end):
                                mutant = False
                            if locid[locus_name].target_start <= start <= locid[locus_name].target_end or locid[locus_name].target_start <= end <= locid[locus_name].target_end:
                                within_target_wt = False
                if within_target and within_target_wt:
                    if two_pass == 1:
                        pair_status.append('aligned')
                    else:
                        pair_status.append('wt')
                elif mutant:
                    pair_allele.append(indels)
                    pair_status.append('mutant')
                else:
                    pair_status.append('aligned')
                if verbose:
                    print('Read', iread, read.qname, read.rname, read.cigar_string)
                    aln_ref, aln_read, aln_symbols = read.get_read_aln()
                    print(('0123456789' * math.ceil(len(aln_symbols) / 10.))[:len(aln_symbols)])
                    print(aln_ref)
                    print(aln_symbols)
                    print(aln_read)
                iread += 1
        if verbose:
            print('Pair', pair_status, pair_allele)

        if two_pass == 1:
            if pair_status.count('aligned') > 0:
                first_pass_out[read.rname].write('>%s\n%s\n>%s\n%s\n'%(r1.qname, r1.seq, r2.qname, r2.seq))
        elif two_pass == 0 or two_pass == 2:
            if pair_status.count('mutant') > 0:
                loci_npair_mutant[locus_name] += 1
                # Merging events: taking all read1 events and completing them with unique read2 events
                if len(pair_allele) > 1:
                    if r1.pos <= r2.pos:
                        # Add read1 events
                        pair_allele_merged = pair_allele[0]
                        # Add read2 events
                        # End of last indel detected on read1
                        end_last = pair_allele[0][-1][-1]
                        for etype, start, end in pair_allele[1]:
                            if end_last < start:
                                pair_allele_merged.append((etype, start, end))
                    else:
                        # Add read2 events
                        pair_allele_merged = []
                        # Start of first indel detected on read1
                        start_first = pair_allele[1][0][1]
                        for etype, start, end in pair_allele[1]:
                            if end < start_first:
                                pair_allele_merged.append((etype, start, end))
                        # Add read1 events
                        pair_allele_merged.extend(pair_allele[1])
                else:
                    pair_allele_merged = pair_allele[0]
                if verbose:
                    print('Pair merged', pair_allele_merged)
                # Adding allele
                if tuple(pair_allele_merged) in loci_alleles[locus_name]:
                    loci_alleles[locus_name][tuple(pair_allele_merged)] += 1
                else:
                    loci_alleles[locus_name][tuple(pair_allele_merged)] = 1
            elif pair_status.count('wt') > 0:
                loci_npair_wt[locus_name] += 1
            elif pair_status.count('aligned') > 0:
                loci_npair_aligned[locus_name] += 1
            loci_npair[locus_name] += 1

    if two_pass == 1:
        for f in first_pass_out.values():
            f.close()

    return loci_alleles, loci_npair, loci_npair_mutant, loci_npair_aligned, loci_npair_wt, npair_unaligned
