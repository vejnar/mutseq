# Mut-Seq: CRISPR-Cas9 Mutagenesis Allele Frequency

The *Mut-Seq* library is a Python library measuring allele frequency in high-throughput sequencing reads.

## Overview

The protocol is published [Optimized CRISPR-Cas9 System for Genome Editing in Zebrafish](http://cshprotocols.cshlp.org/content/2016/10/pdb.prot086850.short) in journal *Cold Spring Harbor Protocol*.

Using CRISPR-Cas9 system *in vivo* is introduced in [Optimization Strategies for the CRISPR–Cas9 Genome-Editing System](http://cshprotocols.cshlp.org/content/2016/10/pdb.top090894.short) in the same journal.

See [Mut-Seq web site](http://protocol.crisprscan.org) for installation and usage details.

## Download

See [tags](/../tags) page.

## Licence

The *Mut-Seq* library is distributed under the Mozilla Public License Version 2.0 (see /LICENCE).

Copyright (C) 2015-2017 Charles E. Vejnar

## Citing

```
Optimized CRISPR-Cas9 System for Genome Editing in Zebrafish.
Vejnar CE, Moreno-Mateos MA, Cifuentes D, Bazzini AA, Giraldez AJ
Cold Spring Harb Protoc. 2016 Oct 3;2016(10)
PMID:27698232, DOI:10.1101/pdb.prot086850
```
